﻿using System.Reflection;

[assembly: AssemblyTitle("Tecan.Sila2.DynamicClient")]
[assembly: AssemblyDescription("A generic client interface that can communicate with any SiLA2 Server without recompile")]