﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Tecan.Sila2.Locking")]
[assembly: AssemblyDescription("Generic server-side Locking support")]

[assembly: InternalsVisibleTo( "Tecan.Sila2.Locking.Tests" )]