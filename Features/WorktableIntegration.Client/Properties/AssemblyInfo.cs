﻿using System.Reflection;

[assembly: AssemblyTitle( "Tecan.Sila2.WorktableIntegration.Client" )]
[assembly: AssemblyDescription("Library to read worktable integration information")]