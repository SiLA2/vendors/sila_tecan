﻿using System.Reflection;

[assembly: AssemblyTitle("Tecan.Sila2.WorktableIntegration")]
[assembly: AssemblyDescription("Server-side support for worktable integration")]