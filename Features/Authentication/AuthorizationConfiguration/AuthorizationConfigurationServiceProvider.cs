//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Linq;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Tecan.Sila2.Authorization
{
    
    
    /// <summary>
    /// A class that exposes the IAuthorizationConfigurationService interface via SiLA2
    /// </summary>
    [System.ComponentModel.Composition.ExportAttribute(typeof(IFeatureProvider))]
    [System.ComponentModel.Composition.PartCreationPolicyAttribute(System.ComponentModel.Composition.CreationPolicy.Shared)]
    public partial class AuthorizationConfigurationServiceProvider : IFeatureProvider
    {
        
        private IAuthorizationConfigurationService _implementation;
        
        private Tecan.Sila2.Server.ISiLAServer _server;
        
        private static Tecan.Sila2.Feature _feature = FeatureSerializer.LoadFromAssembly(typeof(AuthorizationConfigurationServiceProvider).Assembly, "AuthorizationConfigurationService.sila.xml");
        
        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="implementation">The implementation to exported through SiLA2</param>
        /// <param name="server">The SiLA2 server instance through which the implementation shall be exported</param>
        [System.ComponentModel.Composition.ImportingConstructorAttribute()]
        public AuthorizationConfigurationServiceProvider(IAuthorizationConfigurationService implementation, Tecan.Sila2.Server.ISiLAServer server)
        {
            _implementation = implementation;
            _server = server;
        }
        
        /// <summary>
        /// The feature that is exposed by this feature provider
        /// </summary>
        /// <returns>A feature object</returns>
        public Tecan.Sila2.Feature FeatureDefinition
        {
            get
            {
                return _feature;
            }
        }
        
        /// <summary>
        /// Registers the feature in the provided feature registration
        /// </summary>
        /// <param name="registration">The registration component to which the feature should be registered</param>
        public void Register(IServerBuilder registration)
        {
            registration.RegisterUnobservableCommand<SetAuthorizationProviderRequestDto, EmptyRequest>("SetAuthorizationProvider", SetAuthorizationProvider);
            registration.RegisterUnobservableProperty("AuthorizationProvider", GetAuthorizationProvider);
        }
        
        /// <summary>
        /// Executes the Set Authorization Provider command
        /// </summary>
        /// <param name="request">A data transfer object that contains the command parameters</param>
        /// <returns>The command response wrapped in a data transfer object</returns>
        protected virtual EmptyRequest SetAuthorizationProvider(SetAuthorizationProviderRequestDto request)
        {
            try
            {
                request.Validate();
                _implementation.SetAuthorizationProvider(request.AuthorizationProvider.TryExtract(_server, "org.silastandard/core/AuthorizationConfigurationService/v1/Command/SetAuthorizati" +
                            "onProvider/Parameter/AuthorizationProvider"));
                return EmptyRequest.Instance;
            } catch (System.ArgumentException ex)
            {
                if ((ex.ParamName == "authorizationProvider"))
                {
                    throw _server.ErrorHandling.CreateValidationError("org.silastandard/core/AuthorizationConfigurationService/v1/Command/SetAuthorizati" +
                            "onProvider/Parameter/AuthorizationProvider", ex.Message);
                }
                throw _server.ErrorHandling.CreateUnknownValidationError(ex);
            }
        }
        
        /// <summary>
        /// Gets the current value of the Authorization Provider property
        /// </summary>
        /// <returns>The current value wrapped in a data transfer object</returns>
        protected virtual AuthorizationProviderResponseDto GetAuthorizationProvider()
        {
            return new AuthorizationProviderResponseDto(_implementation.AuthorizationProvider, _server);
        }
        
        /// <summary>
        /// Gets the command with the given identifier
        /// </summary>
        /// <param name="commandIdentifier">A fully qualified command identifier</param>
        /// <returns>A method object or null, if the command is not supported</returns>
        public System.Reflection.MethodInfo GetCommand(string commandIdentifier)
        {
            if ((commandIdentifier == "org.silastandard/core/AuthorizationConfigurationService/v1/Command/SetAuthorizati" +
                "onProvider"))
            {
                return typeof(IAuthorizationConfigurationService).GetMethod("SetAuthorizationProvider");
            }
            return null;
        }
        
        /// <summary>
        /// Gets the property with the given identifier
        /// </summary>
        /// <param name="propertyIdentifier">A fully qualified property identifier</param>
        /// <returns>A property object or null, if the property is not supported</returns>
        public System.Reflection.PropertyInfo GetProperty(string propertyIdentifier)
        {
            if ((propertyIdentifier == "org.silastandard/core/AuthorizationConfigurationService/v1/Property/Authorization" +
                "Provider"))
            {
                return typeof(IAuthorizationConfigurationService).GetProperty("AuthorizationProvider");
            }
            return null;
        }
    }
}

