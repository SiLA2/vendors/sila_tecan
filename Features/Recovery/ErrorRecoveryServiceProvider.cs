//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Linq;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Tecan.Sila2.Recovery
{
    
    
    /// <summary>
    /// A class that exposes the IErrorRecoveryService interface via SiLA2
    /// </summary>
    [System.ComponentModel.Composition.ExportAttribute(typeof(IFeatureProvider))]
    [System.ComponentModel.Composition.PartCreationPolicyAttribute(System.ComponentModel.Composition.CreationPolicy.Shared)]
    public partial class ErrorRecoveryServiceProvider : IFeatureProvider
    {
        
        private IErrorRecoveryService _implementation;
        
        private Tecan.Sila2.Server.ISiLAServer _server;
        
        private static Tecan.Sila2.Feature _feature = FeatureSerializer.LoadFromAssembly(typeof(ErrorRecoveryServiceProvider).Assembly, "ErrorRecoveryService.sila.xml");
        
        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="implementation">The implementation to exported through SiLA2</param>
        /// <param name="server">The SiLA2 server instance through which the implementation shall be exported</param>
        [System.ComponentModel.Composition.ImportingConstructorAttribute()]
        public ErrorRecoveryServiceProvider(IErrorRecoveryService implementation, Tecan.Sila2.Server.ISiLAServer server)
        {
            _implementation = implementation;
            _server = server;
        }
        
        /// <summary>
        /// The feature that is exposed by this feature provider
        /// </summary>
        /// <returns>A feature object</returns>
        public Tecan.Sila2.Feature FeatureDefinition
        {
            get
            {
                return _feature;
            }
        }
        
        /// <summary>
        /// Registers the feature in the provided feature registration
        /// </summary>
        /// <param name="registration">The registration component to which the feature should be registered</param>
        public void Register(IServerBuilder registration)
        {
            registration.RegisterUnobservableCommand<ExecuteContinuationOptionRequestDto, EmptyRequest>("ExecuteContinuationOption", ExecuteContinuationOption);
            registration.RegisterUnobservableCommand<AbortErrorHandlingRequestDto, EmptyRequest>("AbortErrorHandling", AbortErrorHandling);
            registration.RegisterUnobservableCommand<SetErrorHandlingTimeoutRequestDto, EmptyRequest>("SetErrorHandlingTimeout", SetErrorHandlingTimeout);
            registration.RegisterObservableProperty("RecoverableErrors", GetRecoverableErrors, _implementation);
        }
        
        /// <summary>
        /// Executes the Execute Continuation Option command
        /// </summary>
        /// <param name="request">A data transfer object that contains the command parameters</param>
        /// <returns>The command response wrapped in a data transfer object</returns>
        protected virtual EmptyRequest ExecuteContinuationOption(ExecuteContinuationOptionRequestDto request)
        {
            try
            {
                request.Validate();
                _implementation.ExecuteContinuationOption(request.CommandExecutionUUID.TryExtract(_server, "org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption/P" +
                            "arameter/CommandExecutionUUID"), request.ContinuationOption.TryExtract(_server, "org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption/P" +
                            "arameter/ContinuationOption"), request.InputData.TryExtract(_server, "org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption/P" +
                            "arameter/InputData"));
                return EmptyRequest.Instance;
            } catch (System.ArgumentException ex)
            {
                if ((ex.ParamName == "commandExecutionUUID"))
                {
                    throw _server.ErrorHandling.CreateValidationError("org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption/P" +
                            "arameter/CommandExecutionUUID", ex.Message);
                }
                if ((ex.ParamName == "continuationOption"))
                {
                    throw _server.ErrorHandling.CreateValidationError("org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption/P" +
                            "arameter/ContinuationOption", ex.Message);
                }
                if ((ex.ParamName == "inputData"))
                {
                    throw _server.ErrorHandling.CreateValidationError("org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption/P" +
                            "arameter/InputData", ex.Message);
                }
                throw _server.ErrorHandling.CreateUnknownValidationError(ex);
            } catch (InvalidCommandExecutionUUIDException ex)
            {
                throw _server.ErrorHandling.CreateExecutionError("org.silastandard/core/ErrorRecoveryService/v1/DefinedExecutionError/InvalidComman" +
                        "dExecutionUUID", @"
      The specified command execution UUID is not valid for error recovery. There is currently no unhandled recoverable error related
      to the specified command execution. A possibly occurred error might have been handled already by another client.
    ", ex.Message);
            } catch (UnknownContinuationOptionException ex)
            {
                throw _server.ErrorHandling.CreateExecutionError("org.silastandard/core/ErrorRecoveryService/v1/DefinedExecutionError/UnknownContin" +
                        "uationOption", "The specified continuation option is not defined for the error of the given obser" +
                        "vable command execution.", ex.Message);
            }
        }
        
        /// <summary>
        /// Executes the Abort Error Handling command
        /// </summary>
        /// <param name="request">A data transfer object that contains the command parameters</param>
        /// <returns>The command response wrapped in a data transfer object</returns>
        protected virtual EmptyRequest AbortErrorHandling(AbortErrorHandlingRequestDto request)
        {
            try
            {
                request.Validate();
                _implementation.AbortErrorHandling(request.CommandExecutionUUID.TryExtract(_server, "org.silastandard/core/ErrorRecoveryService/v1/Command/AbortErrorHandling/Paramete" +
                            "r/CommandExecutionUUID"));
                return EmptyRequest.Instance;
            } catch (System.ArgumentException ex)
            {
                if ((ex.ParamName == "commandExecutionUUID"))
                {
                    throw _server.ErrorHandling.CreateValidationError("org.silastandard/core/ErrorRecoveryService/v1/Command/AbortErrorHandling/Paramete" +
                            "r/CommandExecutionUUID", ex.Message);
                }
                throw _server.ErrorHandling.CreateUnknownValidationError(ex);
            } catch (InvalidCommandExecutionUUIDException ex)
            {
                throw _server.ErrorHandling.CreateExecutionError("org.silastandard/core/ErrorRecoveryService/v1/DefinedExecutionError/InvalidComman" +
                        "dExecutionUUID", @"
      The specified command execution UUID is not valid for error recovery. There is currently no unhandled recoverable error related
      to the specified command execution. A possibly occurred error might have been handled already by another client.
    ", ex.Message);
            }
        }
        
        /// <summary>
        /// Executes the Set Error Handling Timeout command
        /// </summary>
        /// <param name="request">A data transfer object that contains the command parameters</param>
        /// <returns>The command response wrapped in a data transfer object</returns>
        protected virtual EmptyRequest SetErrorHandlingTimeout(SetErrorHandlingTimeoutRequestDto request)
        {
            try
            {
                request.Validate();
                _implementation.SetErrorHandlingTimeout(request.ErrorHandlingTimeout.TryExtract(_server, "org.silastandard/core/ErrorRecoveryService/v1/Command/SetErrorHandlingTimeout/Par" +
                            "ameter/ErrorHandlingTimeout"));
                return EmptyRequest.Instance;
            } catch (System.ArgumentException ex)
            {
                if ((ex.ParamName == "errorHandlingTimeout"))
                {
                    throw _server.ErrorHandling.CreateValidationError("org.silastandard/core/ErrorRecoveryService/v1/Command/SetErrorHandlingTimeout/Par" +
                            "ameter/ErrorHandlingTimeout", ex.Message);
                }
                throw _server.ErrorHandling.CreateUnknownValidationError(ex);
            }
        }
        
        /// <summary>
        /// Gets the current value of the Recoverable Errors property
        /// </summary>
        /// <returns>The current value wrapped in a data transfer object</returns>
        protected virtual PropertyResponse<System.Collections.Generic.List<RecoverableErrorDto>> GetRecoverableErrors()
        {
            return new PropertyResponse<System.Collections.Generic.List<RecoverableErrorDto>>(Tecan.Sila2.DtoExtensions.Encapsulate(_implementation.RecoverableErrors, RecoverableErrorDto.Create, _server));
        }
        
        /// <summary>
        /// Gets the command with the given identifier
        /// </summary>
        /// <param name="commandIdentifier">A fully qualified command identifier</param>
        /// <returns>A method object or null, if the command is not supported</returns>
        public System.Reflection.MethodInfo GetCommand(string commandIdentifier)
        {
            if ((commandIdentifier == "org.silastandard/core/ErrorRecoveryService/v1/Command/ExecuteContinuationOption"))
            {
                return typeof(IErrorRecoveryService).GetMethod("ExecuteContinuationOption");
            }
            if ((commandIdentifier == "org.silastandard/core/ErrorRecoveryService/v1/Command/AbortErrorHandling"))
            {
                return typeof(IErrorRecoveryService).GetMethod("AbortErrorHandling");
            }
            if ((commandIdentifier == "org.silastandard/core/ErrorRecoveryService/v1/Command/SetErrorHandlingTimeout"))
            {
                return typeof(IErrorRecoveryService).GetMethod("SetErrorHandlingTimeout");
            }
            return null;
        }
        
        /// <summary>
        /// Gets the property with the given identifier
        /// </summary>
        /// <param name="propertyIdentifier">A fully qualified property identifier</param>
        /// <returns>A property object or null, if the property is not supported</returns>
        public System.Reflection.PropertyInfo GetProperty(string propertyIdentifier)
        {
            if ((propertyIdentifier == "org.silastandard/core/ErrorRecoveryService/v1/Property/RecoverableErrors"))
            {
                return typeof(IErrorRecoveryService).GetProperty("RecoverableErrors");
            }
            return null;
        }
    }
}

