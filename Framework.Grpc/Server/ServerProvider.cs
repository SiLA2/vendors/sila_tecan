﻿using Common.Logging;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;
using Tecan.Sila2.Security;
using GrpcServer = Grpc.Core.Server;

namespace Tecan.Sila2
{
    /// <summary>
    /// Denotes a component that provides a gRPC server
    /// </summary>
    [Export( typeof( IServerProvider ) )]
    [PartCreationPolicy( CreationPolicy.Shared )]
    public class ServerProvider : IServerProvider
    {
        private readonly ILog _logger = LogManager.GetLogger<ServerProvider>();
        private readonly GrpcServer _server = new GrpcServer();
        private readonly IServerCertificateProvider _certificateProvider;

        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="certificateProvider">the certificate provider</param>
        [ImportingConstructor]
        public ServerProvider( IServerCertificateProvider certificateProvider )
        {
            _certificateProvider = certificateProvider;
        }

        /// <inheritdoc />
        public ServerCredentials Credentials { get; set; }

        /// <inheritdoc />
        public void AddService( ServerServiceDefinition serviceDefinition )
        {
            _server.Services.Add( serviceDefinition );
        }

        /// <inheritdoc />
        public void Configure( string host, int port )
        {
            if(Credentials == null)
            {
                var context = _certificateProvider.CreateContext();
                if(context != null)
                {
                    Credentials = CreateCredentials( context ) ?? ServerCredentials.Insecure;
                }
                else
                {
                    _logger.Info( $"No certificates configured. Server will start with plain-text communication" );
                    Credentials = ServerCredentials.Insecure;
                }
            }

            _server.Ports.Add( new ServerPort( host, port, Credentials ) );
        }

        /// <summary>
        /// Creates the server credentials for the given PEM-encoded certificates
        /// </summary>
        /// <param name="pemContext">The PEM-encoded context</param>
        /// <returns>the server credentials to be used</returns>
        protected virtual ServerCredentials CreateCredentials( CertificateContext pemContext )
        {
            if(pemContext.CertificateAuthority == null) return FallBackDueTo( "CA" );
            if(pemContext.Certificate == null) return FallBackDueTo( "server certificate" );
            if(pemContext.Key == null) return FallBackDueTo( "server key" );

            var keypair = new KeyCertificatePair( pemContext.Certificate, pemContext.Key );
            _logger.Info( "Found certificates, starting server with TLS." );
            return new SslServerCredentials( new List<KeyCertificatePair>() { keypair }, pemContext.CertificateAuthority, SslClientCertificateRequestType.DontRequest );
        }

        private ServerCredentials FallBackDueTo( string missing )
        {
            _logger.Warn( $"The {missing} is missing. The server will start with plain-text communication." );
            return ServerCredentials.Insecure;
        }

        /// <inheritdoc />
        public void ConfigureForServer( Guid guid, int port, Predicate<NetworkInterface> networkInterfaceFilter )
        {
            if(Credentials == null)
            {
                var context = _certificateProvider.CreateContext( guid );
                if(context != null)
                {
                    Credentials = CreateCredentials( context ) ?? ServerCredentials.Insecure;
                }
                else
                {
                    _logger.Warn( $"No certificates found. Server will start with plain-text communication" );
                    Credentials = ServerCredentials.Insecure;
                }
            }

            if(networkInterfaceFilter != null)
            {
                _logger.Info( "Running on the following addresses:" );
                foreach(var v4addr in NetworkInterface.GetAllNetworkInterfaces()
                    .Where( nic => networkInterfaceFilter( nic ) )
                    .SelectMany( nic => nic.GetIPProperties().UnicastAddresses.Select( ua => ua.Address ) )
                    .Where( a => a.AddressFamily == AddressFamily.InterNetwork ))
                {
                    _logger.Info( v4addr.ToString() );
                    _server.Ports.Add( new ServerPort( v4addr.ToString(), port, Credentials ) );
                }
            }
            else
            {
                // if no interface was specified, the SiLA server will (try to) run on all of them
                _server.Ports.Add( new ServerPort( "0.0.0.0", port, Credentials ) );
            }
        }

        /// <inheritdoc />
        public Task ShutdownAsync()
        {
            _logger.Info( "Shutting down gRPC server" );
            return _server.ShutdownAsync();
        }

        /// <inheritdoc />
        public void Start()
        {
            _logger.Info( "Starting gRPC server" );
            _server.Start();
        }
    }
}
