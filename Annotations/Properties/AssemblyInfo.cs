﻿using System.Reflection;

[assembly: AssemblyTitle( "Tecan.Sila2.Annotations" )]
[assembly: AssemblyDescription( "Attributes to define an interface to be exported via SiLA2" )]