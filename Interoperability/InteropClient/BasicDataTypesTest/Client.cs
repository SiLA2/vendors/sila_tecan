//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Linq;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Tecan.Sila2.Interop.Client.BasicDataTypesTest.BasicDataTypesTest
{
    
    
    /// <summary>
    /// Class that implements the IBasicDataTypesTest interface through SiLA2
    /// </summary>
    public partial class BasicDataTypesTestClient : IBasicDataTypesTest
    {
        
        private System.Lazy<string> _stringValue;
        
        private System.Lazy<long> _integerValue;
        
        private System.Lazy<double> _realValue;
        
        private System.Lazy<bool> _booleanValue;
        
        private System.Lazy<System.DateTimeOffset> _dateValue;
        
        private System.Lazy<System.TimeSpan> _timeValue;
        
        private System.Lazy<System.DateTimeOffset> _timestampValue;
        
        private IClientExecutionManager _executionManager;
        
        private IClientChannel _channel;
        
        private const string _serviceName = "sila2.org.silastandard.test.basicdatatypestest.v1.BasicDataTypesTest";
        
        /// <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="channel">The channel through which calls should be executed</param>
        /// <param name="executionManager">A component to determine metadata to attach to any requests</param>
        public BasicDataTypesTestClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            _executionManager = executionManager;
            _channel = channel;
            InitLazyRequests();
        }
        
        /// <summary>
        /// Returns the String value 'SiLA2_Test_String_Value'.
        /// </summary>
        public virtual string StringValue
        {
            get
            {
                return _stringValue.Value;
            }
        }
        
        /// <summary>
        /// Returns the Integer value 5124.
        /// </summary>
        public virtual long IntegerValue
        {
            get
            {
                return _integerValue.Value;
            }
        }
        
        /// <summary>
        /// Returns the Real value 3.1415926.
        /// </summary>
        public virtual double RealValue
        {
            get
            {
                return _realValue.Value;
            }
        }
        
        /// <summary>
        /// Returns the Boolean value true.
        /// </summary>
        public virtual bool BooleanValue
        {
            get
            {
                return _booleanValue.Value;
            }
        }
        
        /// <summary>
        /// Returns the Date value 05.08.2022 respective 08/05/2018, timezone +2.
        /// </summary>
        public virtual System.DateTimeOffset DateValue
        {
            get
            {
                return _dateValue.Value;
            }
        }
        
        /// <summary>
        /// Returns the Time value 12:34:56, timezone +2.
        /// </summary>
        public virtual System.TimeSpan TimeValue
        {
            get
            {
                return _timeValue.Value;
            }
        }
        
        /// <summary>
        /// Returns the Timestamp value 2022-08-05 12:34:56, timezone +2.
        /// </summary>
        public virtual System.DateTimeOffset TimestampValue
        {
            get
            {
                return _timestampValue.Value;
            }
        }
        
        private string RequestStringValue()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Property/StringValue");
            try
            {
                string response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.StringDto>>(_serviceName, "StringValue", "org.silastandard/test/BasicDataTypesTest/v1/Property/StringValue", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private long RequestIntegerValue()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Property/IntegerValue");
            try
            {
                long response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.IntegerDto>>(_serviceName, "IntegerValue", "org.silastandard/test/BasicDataTypesTest/v1/Property/IntegerValue", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private double RequestRealValue()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Property/RealValue");
            try
            {
                double response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.RealDto>>(_serviceName, "RealValue", "org.silastandard/test/BasicDataTypesTest/v1/Property/RealValue", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private bool RequestBooleanValue()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Property/BooleanValue");
            try
            {
                bool response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.BooleanDto>>(_serviceName, "BooleanValue", "org.silastandard/test/BasicDataTypesTest/v1/Property/BooleanValue", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private System.DateTimeOffset RequestDateValue()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Property/DateValue");
            try
            {
                System.DateTimeOffset response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.DateDto>>(_serviceName, "DateValue", "org.silastandard/test/BasicDataTypesTest/v1/Property/DateValue", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private System.TimeSpan RequestTimeValue()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Property/TimeValue");
            try
            {
                System.TimeSpan response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.TimeDto>>(_serviceName, "TimeValue", "org.silastandard/test/BasicDataTypesTest/v1/Property/TimeValue", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private System.DateTimeOffset RequestTimestampValue()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Property/TimestampValue");
            try
            {
                System.DateTimeOffset response = _channel.ReadProperty<PropertyResponse<Tecan.Sila2.TimestampDto>>(_serviceName, "TimestampValue", "org.silastandard/test/BasicDataTypesTest/v1/Property/TimestampValue", callInfo).Value.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Initializes lazies for non-observable properties.
        /// </summary>
        private void InitLazyRequests()
        {
            _stringValue = new System.Lazy<string>(RequestStringValue);
            _integerValue = new System.Lazy<long>(RequestIntegerValue);
            _realValue = new System.Lazy<double>(RequestRealValue);
            _booleanValue = new System.Lazy<bool>(RequestBooleanValue);
            _dateValue = new System.Lazy<System.DateTimeOffset>(RequestDateValue);
            _timeValue = new System.Lazy<System.TimeSpan>(RequestTimeValue);
            _timestampValue = new System.Lazy<System.DateTimeOffset>(RequestTimestampValue);
        }
        
        /// <summary>
        /// Receives a String value and returns the String value that has been received.
        /// </summary>
        /// <param name="stringValue">The String value to be returned.</param>
        public virtual string EchoStringValue(string stringValue)
        {
            EchoStringValueRequestDto request = new EchoStringValueRequestDto(stringValue, null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Command/EchoStringValue");
            try
            {
                string response = _channel.ExecuteUnobservableCommand<EchoStringValueRequestDto, EchoStringValueResponseDto>(_serviceName, "EchoStringValue", request, callInfo).ReceivedValue.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Receives an Integer value and returns the Integer value that has been received.
        /// </summary>
        /// <param name="integerValue">The Integer value to be returned.</param>
        public virtual long EchoIntegerValue(long integerValue)
        {
            EchoIntegerValueRequestDto request = new EchoIntegerValueRequestDto(integerValue, null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Command/EchoIntegerValue");
            try
            {
                long response = _channel.ExecuteUnobservableCommand<EchoIntegerValueRequestDto, EchoIntegerValueResponseDto>(_serviceName, "EchoIntegerValue", request, callInfo).ReceivedValue.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Receives a Real value and returns the Real value that has been received.
        /// </summary>
        /// <param name="realValue">The Real value to be returned.</param>
        public virtual double EchoRealValue(double realValue)
        {
            EchoRealValueRequestDto request = new EchoRealValueRequestDto(realValue, null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Command/EchoRealValue");
            try
            {
                double response = _channel.ExecuteUnobservableCommand<EchoRealValueRequestDto, EchoRealValueResponseDto>(_serviceName, "EchoRealValue", request, callInfo).ReceivedValue.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Receives a Boolean value and returns the Boolean value that has been received.
        /// </summary>
        /// <param name="booleanValue">The Boolean value to be returned.</param>
        public virtual bool EchoBooleanValue(bool booleanValue)
        {
            EchoBooleanValueRequestDto request = new EchoBooleanValueRequestDto(booleanValue, null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Command/EchoBooleanValue");
            try
            {
                bool response = _channel.ExecuteUnobservableCommand<EchoBooleanValueRequestDto, EchoBooleanValueResponseDto>(_serviceName, "EchoBooleanValue", request, callInfo).ReceivedValue.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Receives a Date value and returns the Date value that has been received.
        /// </summary>
        /// <param name="dateValue">The Date value to be returned.</param>
        public virtual System.DateTimeOffset EchoDateValue(System.DateTimeOffset dateValue)
        {
            EchoDateValueRequestDto request = new EchoDateValueRequestDto(dateValue, null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Command/EchoDateValue");
            try
            {
                System.DateTimeOffset response = _channel.ExecuteUnobservableCommand<EchoDateValueRequestDto, EchoDateValueResponseDto>(_serviceName, "EchoDateValue", request, callInfo).ReceivedValue.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Receives a Time value and returns the Time value that has been received.
        /// </summary>
        /// <param name="timeValue">The Time value to be returned.</param>
        public virtual System.TimeSpan EchoTimeValue(System.TimeSpan timeValue)
        {
            EchoTimeValueRequestDto request = new EchoTimeValueRequestDto(timeValue, null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Command/EchoTimeValue");
            try
            {
                System.TimeSpan response = _channel.ExecuteUnobservableCommand<EchoTimeValueRequestDto, EchoTimeValueResponseDto>(_serviceName, "EchoTimeValue", request, callInfo).ReceivedValue.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        /// <summary>
        /// Receives a Timestamp value and returns a message containing the Timestamp value that has been received.
        /// </summary>
        /// <param name="timestampValue">The Timestamp value to be returned.</param>
        public virtual System.DateTimeOffset EchoTimestampValue(System.DateTimeOffset timestampValue)
        {
            EchoTimestampValueRequestDto request = new EchoTimestampValueRequestDto(timestampValue, null);
            request.Validate();
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("org.silastandard/test/BasicDataTypesTest/v1/Command/EchoTimestampValue");
            try
            {
                System.DateTimeOffset response = _channel.ExecuteUnobservableCommand<EchoTimestampValueRequestDto, EchoTimestampValueResponseDto>(_serviceName, "EchoTimestampValue", request, callInfo).ReceivedValue.Extract(_executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        private T Extract<T>(Tecan.Sila2.ISilaTransferObject<T> dto)
        
        {
            return dto.Extract(_executionManager.DownloadBinaryStore);
        }
    }
    
    /// <summary>
    /// Factory to instantiate clients for the Basic Data Types Test.
    /// </summary>
    [System.ComponentModel.Composition.ExportAttribute(typeof(IClientFactory))]
    [System.ComponentModel.Composition.PartCreationPolicyAttribute(System.ComponentModel.Composition.CreationPolicy.Shared)]
    public partial class BasicDataTypesTestClientFactory : IClientFactory
    {
        
        /// <summary>
        /// Gets the fully-qualified identifier of the feature for which clients can be generated
        /// </summary>
        public string FeatureIdentifier
        {
            get
            {
                return "org.silastandard/test/BasicDataTypesTest/v1";
            }
        }
        
        /// <summary>
        /// Gets the interface type for which clients can be generated
        /// </summary>
        public System.Type InterfaceType
        {
            get
            {
                return typeof(IBasicDataTypesTest);
            }
        }
        
        /// <summary>
        /// Creates a strongly typed client for the given execution channel and execution manager
        /// </summary>
        /// <param name="channel">The channel that should be used for communication with the server</param>
        /// <param name="executionManager">The execution manager to manage metadata</param>
        /// <returns>A strongly typed client. This object will be an instance of the InterfaceType property</returns>
        public object CreateClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            return new BasicDataTypesTestClient(channel, executionManager);
        }
    }
}

