//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tecan.Sila2.Interop.Client.AuthenticationTest
{
    
    
    /// <summary>
    /// Contains a command that requires authentication. A client should be able to obtain an authorization Token through a login using the following credentials: username: 'test', password: 'test'
    /// </summary>
    [Tecan.Sila2.SilaFeatureAttribute(true, "test")]
    [Tecan.Sila2.SilaIdentifierAttribute("AuthenticationTest")]
    [Tecan.Sila2.SilaDisplayNameAttribute("Authentication Test")]
    public partial interface IAuthenticationTest
    {
        
        /// <summary>
        /// Requires an authorization token in order to be executed
        /// </summary>
        void RequiresToken();
        
        /// <summary>
        /// Requires an authorization token in order to be executed and to upload a binary parameter
        /// </summary>
        /// <param name="binaryToUpload">A binary that needs to be uploaded</param>
        void RequiresTokenForBinaryUpload(System.IO.Stream binaryToUpload);
    }
}
