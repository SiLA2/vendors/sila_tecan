﻿using System.Reflection;
using Tecan.Sila2;
using Tecan.Sila2.Server;

[assembly: AssemblyTitle( "Tecan.Sila2.InteropServer" )]
[assembly: AssemblyDescription( "An interoperability server implementation using native gRPC" )]

[assembly: VendorUri( "https://gitlab.com/SiLA2/vendors/sila_tecan" )]