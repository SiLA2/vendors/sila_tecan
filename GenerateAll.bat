dotnet build Generator\Generator.csproj -c Release

Build\Generator\net472\SilaGen.exe generate-provider Server\Discovery\SiLAService.sila.xml Framework\Discovery\SilaServiceDto.cs Server\Discovery\SiLAServiceProvider.cs Framework\Discovery\SiLAServiceClient.cs -n Tecan.Sila2.Discovery

Build\Generator\net472\SilaGen.exe generate-interface Server\ConnectionConfigurationService.sila.xml Server\ConnectionConfiguration\IConnectionConfigurationService.cs -n Tecan.Sila2.Server.ConnectionConfiguration
Build\Generator\net472\SilaGen.exe generate-provider Server\ConnectionConfigurationService.sila.xml Server\ConnectionConfiguration\ConnectionConfigurationDtos.cs Server\ConnectionConfiguration\ConnectionConfigurationProvider.cs -n Tecan.Sila2.Server.ConnectionConfiguration -s
Build\Generator\net472\SilaGen.exe generate-client Server\ConnectionConfigurationService.sila.xml Features\ConnectionConfiguration.Client\ConnectionConfiguration.csproj -n Tecan.Sila2.ConnectionConfiguration

Build\Generator\net472\SilaGen.exe generate-interface Features\Locking\LockController.sila.xml Features\Locking\ILockController.cs -n Tecan.Sila2.Locking
Build\Generator\net472\SilaGen.exe generate-provider Features\Locking\LockController.sila.xml Features\Locking\LockControlDtos.cs Features\Locking\LockControlProvider.cs -n Tecan.Sila2.Locking -s

Build\Generator\net472\SilaGen.exe generate-interface Features\Authentication\AuthenticationService.sila.xml Features\Authentication\Authentication\IAuthenticationService.cs -n Tecan.Sila2.Authentication
Build\Generator\net472\SilaGen.exe generate-provider Features\Authentication\AuthenticationService.sila.xml Features\Authentication\Authentication\AuthenticationDtos.cs Features\Authentication\Authentication\AuthenticationProvider.cs -n Tecan.Sila2.Authentication -s
Build\Generator\net472\SilaGen.exe generate-interface Features\Authentication\AuthorizationService.sila.xml Features\Authentication\Authorization\IAuthorizationService.cs -n Tecan.Sila2.Authorization
Build\Generator\net472\SilaGen.exe generate-provider Features\Authentication\AuthorizationService.sila.xml Features\Authentication\Authorization\AuthorizationDtos.cs Features\Authentication\Authorization\AuthorizationProvider.cs -n Tecan.Sila2.Authorization -s
Build\Generator\net472\SilaGen.exe generate-interface Features\Authentication\AuthorizationProviderService.sila.xml Features\Authentication\AuthorizationProvider\IAuthorizationProviderService.cs -n Tecan.Sila2.AuthorizationProvider
Build\Generator\net472\SilaGen.exe generate-provider Features\Authentication\AuthorizationProviderService.sila.xml Features\Authentication\AuthorizationProvider\AuthorizationProviderServiceDtos.cs Features\Authentication\AuthorizationProvider\AuthorizationProviderClient.cs -n Tecan.Sila2.AuthorizationProvider -c
Build\Generator\net472\SilaGen.exe generate-interface Features\Authentication\AuthorizationConfigurationService.sila.xml Features\Authentication\AuthorizationConfiguration\IAuthorizationConfigurationService.cs -n Tecan.Sila2.Authorization
Build\Generator\net472\SilaGen.exe generate-provider Features\Authentication\AuthorizationConfigurationService.sila.xml Features\Authentication\AuthorizationConfiguration\AuthorizationConfigurationServiceDtos.cs Features\Authentication\AuthorizationConfiguration\AuthorizationConfigurationServiceProvider.cs -n Tecan.Sila2.Authorization -s
Build\Generator\net472\SilaGen.exe generate-interface Features\ParameterConstraintsProvider\ParameterConstraintsProvider.sila.xml Features\ParameterConstraintsProvider\IParameterConstraintsProvider.cs -n Tecan.Sila2.ParameterConstraintsProvider
Build\Generator\net472\SilaGen.exe generate-provider Features\ParameterConstraintsProvider\ParameterConstraintsProvider.sila.xml Features\ParameterConstraintsProvider\ParameterConstraintDtos.cs Features\ParameterConstraintsProvider\ParameterConstraintsServiceProvider.cs -n Tecan.Sila2.ParameterConstraintsProvider -s
Build\Generator\net472\SilaGen.exe generate-interface Features\Cancellation\CancelController.sila.xml Features\Cancellation\ICancellationProvider.cs -n Tecan.Sila2.Cancellation
Build\Generator\net472\SilaGen.exe generate-provider Features\Cancellation\CancelController.sila.xml Features\Cancellation\CancellationDtos.cs Features\Cancellation\CancellationProvider.cs -n Tecan.Sila2.Cancellation -s
Build\Generator\net472\SilaGen.exe generate-provider Features\Recovery\ErrorRecoveryService.sila.xml Features\Recovery\ErrorRecoveryServiceDtos.cs Features\Recovery\ErrorRecoveryServiceProvider.cs -s -n Tecan.Sila2.Recovery

Build\Generator\net472\SilaGen.exe generate-server Build\Features\netstandard2.0\Tecan.Sila2.WorktableIntegration.dll Features\WorktableIntegration\WorktableIntegration.csproj -n Tecan.Sila2.WorktableIntegration -v 1.0

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\Logging\Feature.xml Tests\Generator.Tests\Logging\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-provider Tests\Generator.Tests\Logging\Feature.xml Tests\Generator.Tests\Logging\DtoReference.cs Tests\Generator.Tests\Logging\ProviderReference.cs Tests\Generator.Tests\Logging\ClientReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.Logging.ILoggingService Tests\Generator.Tests\Logging\Feature.xml

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\SilaService\Feature.xml Tests\Generator.Tests\SilaService\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-provider Tests\Generator.Tests\SilaService\Feature.xml Tests\Generator.Tests\SilaService\DtoReference.cs Tests\Generator.Tests\SilaService\ProviderReference.cs Tests\Generator.Tests\SilaService\ClientReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.SilaService.ISiLAService Tests\Generator.Tests\SilaService\Feature.xml

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\TemperatureController\Feature.xml Tests\Generator.Tests\TemperatureController\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-provider Tests\Generator.Tests\TemperatureController\Feature.xml Tests\Generator.Tests\TemperatureController\DtoReference.cs Tests\Generator.Tests\TemperatureController\ProviderReference.cs Tests\Generator.Tests\TemperatureController\ClientReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.TemperatureController.ITemperatureController Tests\Generator.Tests\TemperatureController\Feature.xml

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\Dictionary\Feature.xml Tests\Generator.Tests\Dictionary\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-provider Tests\Generator.Tests\Dictionary\Feature.xml Tests\Generator.Tests\Dictionary\DtoReference.cs Tests\Generator.Tests\Dictionary\ProviderReference.cs Tests\Generator.Tests\Dictionary\ClientReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.Dictionary.IDictionaryService Tests\Generator.Tests\Dictionary\Feature.xml

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\InlineObservable\Feature.xml Tests\Generator.Tests\InlineObservable\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.TestReference.IInlineObservableService Tests\Generator.Tests\InlineObservable\Feature.xml

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\Empty\Feature.xml Tests\Generator.Tests\Empty\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-provider Tests\Generator.Tests\Empty\Feature.xml Tests\Generator.Tests\Empty\DtoReference.cs Tests\Generator.Tests\Empty\ProviderReference.cs Tests\Generator.Tests\Empty\ClientReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.Empty.IEmptyService Tests\Generator.Tests\Empty\Feature.xml

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\Rx\Feature.xml Tests\Generator.Tests\Rx\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.TestReference.IRxService Tests\Generator.Tests\Rx\Feature.xml

Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.TestReference.ICentrifuge Tests\Generator.Tests\Centrifuge\Feature.xml

Build\Generator\net472\SilaGen.exe generate-server Build\Test\Features\ParameterDefaultsProvider\net472\Tecan.Sila2.ParameterDefaultsProvider.Tests.dll Tests\Features\ParameterDefaultsProvider.Tests\ParameterDefaultsProvider.Tests.csproj -n Tecan.Sila2.ParameterDefaultsProvider.Tests

Build\Generator\net472\SilaGen.exe generate-server Build\Test\TestServer\net472\TestServer.exe Tests\TestServer\TestServer.csproj -n Tecan.Sila2.IntegrationTests
Build\Generator\net472\SilaGen.exe generate-client Tests\TestServer\TestServer.sila.xml Tests\IntegrationTests\IntegrationTests.csproj -n Tecan.Sila2.IntegrationTests

Build\Generator\net472\SilaGen.exe generate-client Features\Authentication\AuthenticationService.sila.xml Features\Authentication.Client\Authentication.Client.csproj -n Tecan.Sila2.Authentication
Build\Generator\net472\SilaGen.exe generate-client Features\Authentication\AuthorizationConfigurationService.sila.xml Features\Authentication.Client\Authentication.Client.csproj -n Tecan.Sila2.Authorization
Build\Generator\net472\SilaGen.exe generate-client Features\Authentication\AuthorizationProviderService.sila.xml Features\Authentication.Client\Authentication.Client.csproj -n Tecan.Sila2.Authorization
Build\Generator\net472\SilaGen.exe generate-client Features\Locking\LockController.sila.xml Features\Locking.Client\Locking.Client.csproj -n Tecan.Sila2.Locking
Build\Generator\net472\SilaGen.exe generate-client Features\ParameterConstraintsProvider\ParameterConstraintsProvider.sila.xml Features\ParameterConstraintsProvider.Client\ParameterConstraintsProvider.Client.csproj -n Tecan.Sila2.ParameterConstraintsProvider
Build\Generator\net472\SilaGen.exe generate-client Features\WorktableIntegration\WorktableService.sila.xml Features\WorktableIntegration.Client\WorktableIntegration.Client.csproj -n Tecan.Sila2.WorktableIntegration
Build\Generator\net472\SilaGen.exe generate-client Features\Cancellation\CancelController.sila.xml Features\Cancellation.Client\Cancellation.Client.csproj -n Tecan.Sila2.Cancellation

Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\AnyTypeTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.AnyTypeTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\AuthenticationTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.AuthenticationTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\BasicDataTypesTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.BasicDataTypesTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\BinaryTransferTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.BinaryTransferTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\CancellationTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.CancellationTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\ErrorHandlingTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.ErrorHandlingTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\ListDataTypeTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.ListDataTypeTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\MetadataConsumerTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.MetadataConsumerTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\MetadataProvider.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.MetadataProvider
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\MultiClientTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.MultiClientTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\ObservableCommandTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.ObservableCommandTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\ObservablePropertyTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.ObservablePropertyTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\ParameterConstraintsTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.ParameterConstraintsTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\RecoverableErrorProvider.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.RecoverableErrorProvider
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\StructureDataTypeTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.StructureDataTypeTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\UnobservableCommandTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.UnobservableCommandTest
Build\Generator\net472\SilaGen.exe generate-client Interoperability\InteropServer\UnobservablePropertyTest.sila.xml Interoperability\InteropClient\InteropClient.csproj -n Tecan.Sila2.Interop.Client.UnobservablePropertyTest

Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.AnyTypeTest Interoperability\InteropServer\AnyTypeTest.sila.xml Interoperability\InteropServer\AnyTypeTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.AuthenticationTest Interoperability\InteropServer\AuthenticationTest.sila.xml Interoperability\InteropServer\AuthenticationTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.BasicDataTypesTest Interoperability\InteropServer\BasicDataTypesTest.sila.xml Interoperability\InteropServer\BasicDataTypesTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.BinaryTransferTest Interoperability\InteropServer\BinaryTransferTest.sila.xml Interoperability\InteropServer\BinaryTransferTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.CancellationTest Interoperability\InteropServer\CancellationTest.sila.xml Interoperability\InteropServer\CancellationTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.ErrorHandlingTest Interoperability\InteropServer\ErrorHandlingTest.sila.xml Interoperability\InteropServer\ErrorHandlingTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.ListDataTypeTest Interoperability\InteropServer\ListDataTypeTest.sila.xml Interoperability\InteropServer\ListDataTypeTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.MetadataConsumerTest Interoperability\InteropServer\MetadataConsumerTest.sila.xml Interoperability\InteropServer\MetadataConsumerTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.MetadataProvider Interoperability\InteropServer\MetadataProvider.sila.xml Interoperability\InteropServer\MetadataProvider\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.MultiClientTest Interoperability\InteropServer\MultiClientTest.sila.xml Interoperability\InteropServer\MultiClientTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.ObservableCommandTest Interoperability\InteropServer\ObservableCommandTest.sila.xml Interoperability\InteropServer\ObservableCommandTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.ObservablePropertyTest Interoperability\InteropServer\ObservablePropertyTest.sila.xml Interoperability\InteropServer\ObservablePropertyTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.ParameterConstraintsTest Interoperability\InteropServer\ParameterConstraintsTest.sila.xml Interoperability\InteropServer\ParameterConstraintsTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.RecoverableErrorProvider Interoperability\InteropServer\RecoverableErrorProvider.sila.xml Interoperability\InteropServer\RecoverableErrorProvider\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.StructureDataTypeTest Interoperability\InteropServer\StructureDataTypeTest.sila.xml Interoperability\InteropServer\StructureDataTypeTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.UnobservableCommandTest Interoperability\InteropServer\UnobservableCommandTest.sila.xml Interoperability\InteropServer\UnobservableCommandTest\Interface.cs
Build\Generator\net472\SilaGen.exe generate-interface -n Tecan.Sila2.Interop.Server.UnobservablePropertyTest Interoperability\InteropServer\UnobservablePropertyTest.sila.xml Interoperability\InteropServer\UnobservablePropertyTest\Interface.cs

Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.AnyTypeTest Interoperability\InteropServer\AnyTypeTest.sila.xml Interoperability\InteropServer\AnyTypeTest\Dtos.cs Interoperability\InteropServer\AnyTypeTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.AuthenticationTest Interoperability\InteropServer\AuthenticationTest.sila.xml Interoperability\InteropServer\AuthenticationTest\Dtos.cs Interoperability\InteropServer\AuthenticationTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.BasicDataTypesTest Interoperability\InteropServer\BasicDataTypesTest.sila.xml Interoperability\InteropServer\BasicDataTypesTest\Dtos.cs Interoperability\InteropServer\BasicDataTypesTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.BinaryTransferTest Interoperability\InteropServer\BinaryTransferTest.sila.xml Interoperability\InteropServer\BinaryTransferTest\Dtos.cs Interoperability\InteropServer\BinaryTransferTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.CancellationTest Interoperability\InteropServer\CancellationTest.sila.xml Interoperability\InteropServer\CancellationTest\Dtos.cs Interoperability\InteropServer\CancellationTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.ErrorHandlingTest Interoperability\InteropServer\ErrorHandlingTest.sila.xml Interoperability\InteropServer\ErrorHandlingTest\Dtos.cs Interoperability\InteropServer\ErrorHandlingTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.ListDataTypeTest Interoperability\InteropServer\ListDataTypeTest.sila.xml Interoperability\InteropServer\ListDataTypeTest\Dtos.cs Interoperability\InteropServer\ListDataTypeTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.MetadataConsumerTest Interoperability\InteropServer\MetadataConsumerTest.sila.xml Interoperability\InteropServer\MetadataConsumerTest\Dtos.cs Interoperability\InteropServer\MetadataConsumerTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.MetadataProvider Interoperability\InteropServer\MetadataProvider.sila.xml Interoperability\InteropServer\MetadataProvider\Dtos.cs Interoperability\InteropServer\MetadataProvider\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.MultiClientTest Interoperability\InteropServer\MultiClientTest.sila.xml Interoperability\InteropServer\MultiClientTest\Dtos.cs Interoperability\InteropServer\MultiClientTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.ObservableCommandTest Interoperability\InteropServer\ObservableCommandTest.sila.xml Interoperability\InteropServer\ObservableCommandTest\Dtos.cs Interoperability\InteropServer\ObservableCommandTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.ObservablePropertyTest Interoperability\InteropServer\ObservablePropertyTest.sila.xml Interoperability\InteropServer\ObservablePropertyTest\Dtos.cs Interoperability\InteropServer\ObservablePropertyTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.ParameterConstraintsTest Interoperability\InteropServer\ParameterConstraintsTest.sila.xml Interoperability\InteropServer\ParameterConstraintsTest\Dtos.cs Interoperability\InteropServer\ParameterConstraintsTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.RecoverableErrorProvider Interoperability\InteropServer\RecoverableErrorProvider.sila.xml Interoperability\InteropServer\RecoverableErrorProvider\Dtos.cs Interoperability\InteropServer\RecoverableErrorProvider\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.StructureDataTypeTest Interoperability\InteropServer\StructureDataTypeTest.sila.xml Interoperability\InteropServer\StructureDataTypeTest\Dtos.cs Interoperability\InteropServer\StructureDataTypeTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.UnobservableCommandTest Interoperability\InteropServer\UnobservableCommandTest.sila.xml Interoperability\InteropServer\UnobservableCommandTest\Dtos.cs Interoperability\InteropServer\UnobservableCommandTest\Provider.cs -s
Build\Generator\net472\SilaGen.exe generate-provider -n Tecan.Sila2.Interop.Server.UnobservablePropertyTest Interoperability\InteropServer\UnobservablePropertyTest.sila.xml Interoperability\InteropServer\UnobservablePropertyTest\Dtos.cs Interoperability\InteropServer\UnobservablePropertyTest\Provider.cs -s

Build\Generator\net472\SilaGen.exe generate-interface Tests\Generator.Tests\AmbiguityResolver\Feature.xml Tests\Generator.Tests\AmbiguityResolver\ServiceReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-provider Tests\Generator.Tests\AmbiguityResolver\Feature.xml Tests\Generator.Tests\AmbiguityResolver\DtoReference.cs Tests\Generator.Tests\AmbiguityResolver\ProviderReference.cs Tests\Generator.Tests\AmbiguityResolver\ClientReference.cs -n Tecan.Sila2.Generator.Test.TestReference
Build\Generator\net472\SilaGen.exe generate-feature Build\Test\net472\Tecan.Sila2Generator.Test.dll -i Tecan.Sila2.Generator.Test.AmbiguityResolver.IAmbiguityResolverService Tests\Generator.Tests\AmbiguityResolver\Feature.xml --config-file Tests\Generator.Tests\AmbiguityResolver\AmbiguityResolver.xml

xsd /classes Server\ServerConfiguration.xsd /n:Tecan.Sila2.Server /out:Server
xsd /classes Server\ConnectionConfiguration\ClientConnection.xsd /n:Tecan.Sila2.Server.ConnectionConfiguration /out:Server\ConnectionConfiguration
xsd /classes Features\Authentication\AuthConfiguration.xsd /n:Tecan.Sila2 /out:Features\Authentication
xsd /classes Features\Locking\LockingConfiguration.xsd /n:Tecan.Sila2.Locking /out:Features\Locking