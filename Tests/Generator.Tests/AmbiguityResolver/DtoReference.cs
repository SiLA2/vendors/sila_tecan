//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tecan.Sila2;


namespace Tecan.Sila2.Generator.Test.TestReference
{
    
    
    /// <summary>
    /// Data transfer object for the request of the Check Ambiguous Entries command
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class CheckAmbiguousEntriesRequestDto : Tecan.Sila2.ISilaTransferObject, Tecan.Sila2.ISilaRequestObject
    {
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        public CheckAmbiguousEntriesRequestDto()
        {
        }
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="store">An object to organize binaries.</param>
        public CheckAmbiguousEntriesRequestDto(Tecan.Sila2.IBinaryStore store)
        {
        }
        
        /// <summary>
        /// Gets the command identifier for this command
        /// </summary>
        /// <returns>The fully qualified command identifier</returns>
        public string CommandIdentifier
        {
            get
            {
                return "tecan.sila.generator.test/ambiguityresolver/AmbiguityResolverService/v4/Command/C" +
                    "heckAmbiguousEntries";
            }
        }
        
        /// <summary>
        /// Validates the given request object
        /// </summary>
        public void Validate()
        {
        }
        
        /// <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            return null;
        }
    }
    
    /// <summary>
    /// Data transfer object for the response of the Check Ambiguous Entries command
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class CheckAmbiguousEntriesResponseDto : Tecan.Sila2.ISilaTransferObject
    {
        
        private AmbiguousClassDto _returnValue;
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        public CheckAmbiguousEntriesResponseDto()
        {
        }
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="store">An object to organize binaries.</param>
        public CheckAmbiguousEntriesResponseDto(AmbiguousClass returnValue, Tecan.Sila2.IBinaryStore store)
        {
            ReturnValue = new AmbiguousClassDto(returnValue, store);
        }
        
        [ProtoBuf.ProtoMemberAttribute(1)]
        public AmbiguousClassDto ReturnValue
        {
            get
            {
                return _returnValue;
            }
            set
            {
                _returnValue = value;
            }
        }
        
        /// <summary>
        /// Validates the given request object
        /// </summary>
        public void Validate()
        {
            Argument.Validate(ReturnValue, "returnValue");
        }
        
        /// <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            return ("" + Argument.Require(ReturnValue, "returnValue"));
        }
    }
    
    /// <summary>
    /// Data transfer object for the request of the Overloaded Method command
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class OverloadedMethodRequestDto : Tecan.Sila2.ISilaTransferObject, Tecan.Sila2.ISilaRequestObject
    {
        
        private Tecan.Sila2.StringDto _method;
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        public OverloadedMethodRequestDto()
        {
        }
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="store">An object to organize binaries.</param>
        public OverloadedMethodRequestDto(string method, Tecan.Sila2.IBinaryStore store)
        {
            Method = new Tecan.Sila2.StringDto(method, store);
        }
        
        [ProtoBuf.ProtoMemberAttribute(1)]
        public Tecan.Sila2.StringDto Method
        {
            get
            {
                return _method;
            }
            set
            {
                _method = value;
            }
        }
        
        /// <summary>
        /// Gets the command identifier for this command
        /// </summary>
        /// <returns>The fully qualified command identifier</returns>
        public string CommandIdentifier
        {
            get
            {
                return "tecan.sila.generator.test/ambiguityresolver/AmbiguityResolverService/v4/Command/O" +
                    "verloadedMethod";
            }
        }
        
        /// <summary>
        /// Validates the given request object
        /// </summary>
        public void Validate()
        {
            Argument.Validate(Method, "method");
        }
        
        /// <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            return ("" + Argument.Require(Method, "method"));
        }
    }
    
    /// <summary>
    /// The data transfer object for Ambiguous Class
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class AmbiguousClassDto : Tecan.Sila2.ISilaTransferObject<AmbiguousClass>
    {
        
        private InnerStruct _inner;
        
        /// <summary>
        /// Initializes a new instance (to be used by the serializer)
        /// </summary>
        public AmbiguousClassDto()
        {
            _inner = new InnerStruct();
        }
        
        /// <summary>
        /// Initializes a new data transfer object from the business object
        /// </summary>
        /// <param name="inner">The business object that should be transferred</param>
        /// <param name="store">A component to handle binary data</param>
        public AmbiguousClassDto(AmbiguousClass inner, Tecan.Sila2.IBinaryStore store)
        {
            _inner = new InnerStruct(inner, store);
        }
        
        public Tecan.Sila2.StringDto Message
        {
            get
            {
                if ((_inner == null))
                {
                    _inner = new InnerStruct();
                }
                return Inner.Message;
            }
            set
            {
                if ((_inner == null))
                {
                    _inner = new InnerStruct();
                }
                Inner.Message = value;
            }
        }
        
        public Tecan.Sila2.StringDto AmbiguousChannel
        {
            get
            {
                if ((_inner == null))
                {
                    _inner = new InnerStruct();
                }
                return Inner.AmbiguousChannel;
            }
            set
            {
                if ((_inner == null))
                {
                    _inner = new InnerStruct();
                }
                Inner.AmbiguousChannel = value;
            }
        }
        
        /// <summary>
        /// The actual contents of the data transfer object.
        /// </summary>
        [ProtoBuf.ProtoMemberAttribute(1)]
        public InnerStruct Inner
        {
            get
            {
                return _inner;
            }
            set
            {
                _inner = value;
            }
        }
        
        /// <summary>
        /// Extracts the transferred value
        /// </summary>
        /// <param name="store">The binary store in which to store binary data</param>
        /// <returns>the inner value</returns>
        public AmbiguousClass Extract(Tecan.Sila2.IBinaryStore store)
        {
            return new AmbiguousClass(Message.Extract(store), AmbiguousChannel.Extract(store));
        }
        
        /// <summary>
        /// Creates the data transfer object from the given object to transport
        /// </summary>
        /// <param name="inner">The object to transfer</param>
        /// <param name="store">An object to store binary data</param>
        public static AmbiguousClassDto Create(AmbiguousClass inner, Tecan.Sila2.IBinaryStore store)
        {
            return new AmbiguousClassDto(inner, store);
        }
        
        /// <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            return (("" + Argument.Require(Message, "message")) 
                        + Argument.Require(AmbiguousChannel, "ambiguousChannel"));
        }
        
        /// <summary>
        /// Represents the inner structure for actual content
        /// </summary>
        [ProtoBuf.ProtoContractAttribute()]
        public class InnerStruct
        {
            
            private Tecan.Sila2.StringDto _message;
            
            private Tecan.Sila2.StringDto _ambiguousChannel;
            
            /// <summary>
            /// Initializes a new instance (to be used by the serializer)
            /// </summary>
            public InnerStruct()
            {
            }
            
            /// <summary>
            /// Initializes a new data transfer object from the business object
            /// </summary>
            /// <param name="inner">The business object that should be transferred</param>
            /// <param name="store">A component to handle binary data</param>
            public InnerStruct(AmbiguousClass inner, Tecan.Sila2.IBinaryStore store)
            {
                Message = new Tecan.Sila2.StringDto(inner.Message, store);
                AmbiguousChannel = new Tecan.Sila2.StringDto(inner.AmbiguousChannel, store);
            }
            
            [ProtoBuf.ProtoMemberAttribute(1)]
            public Tecan.Sila2.StringDto Message
            {
                get
                {
                    return _message;
                }
                set
                {
                    _message = value;
                }
            }
            
            [ProtoBuf.ProtoMemberAttribute(2)]
            public Tecan.Sila2.StringDto AmbiguousChannel
            {
                get
                {
                    return _ambiguousChannel;
                }
                set
                {
                    _ambiguousChannel = value;
                }
            }
        }
    }
}
