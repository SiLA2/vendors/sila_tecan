﻿using Tecan.Sila2.Server;

namespace Tecan.Sila2.Generator.Test.TestReference
{
    public partial interface ILoggingService
    {
        IRequestInterceptor LoggingActivity
        {
            get;
        }
    }
}
