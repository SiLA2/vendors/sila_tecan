//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tecan.Sila2.Generator.Test.TestReference
{
    
    
    /// <summary>
    /// Provides services for integration of device logging
    /// </summary>
    [Tecan.Sila2.SilaFeatureAttribute(true, "infrastructure")]
    [Tecan.Sila2.SilaIdentifierAttribute("LoggingService")]
    [Tecan.Sila2.SilaDisplayNameAttribute("Logging")]
    public partial interface ILoggingService
    {
        
        /// <summary>
        /// Starts listening for log entries from the device
        /// </summary>
        [Tecan.Sila2.ObservableAttribute()]
        [Tecan.Sila2.SilaDisplayNameAttribute("Listen for log entries")]
        Tecan.Sila2.IIntermediateObservableCommand<LogEntry> ListenForLogEntries();
    }
    
    public class LogEntryData
    {
        
        private string _key;
        
        private string _value;
        
        private System.Collections.Generic.ICollection<LogEntryData> _children;
        
        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public LogEntryData(string key, string value, System.Collections.Generic.ICollection<LogEntryData> children)
        {
            _key = key;
            _value = value;
            _children = children;
        }
        
        public string Key
        {
            get
            {
                return _key;
            }
        }
        
        public string Value
        {
            get
            {
                return _value;
            }
        }
        
        public System.Collections.Generic.ICollection<LogEntryData> Children
        {
            get
            {
                return _children;
            }
        }
    }
    
    /// <summary>
    /// Severity
    /// </summary>
    public enum Severity
    {
        
        None,
        
        Debug,
        
        Info,
        
        Warning,
        
        Error,
        
        Critical,
    }
    
    /// <summary>
    /// Describes a log entry
    /// </summary>
    public class LogEntry
    {
        
        private string _message;
        
        private string _channel;
        
        private Severity _severity;
        
        private System.Collections.Generic.ICollection<LogEntryData> _data;
        
        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public LogEntry(string message, string channel, Severity severity, System.Collections.Generic.ICollection<LogEntryData> data)
        {
            _message = message;
            _channel = channel;
            _severity = severity;
            _data = data;
        }
        
        public string Message
        {
            get
            {
                return _message;
            }
        }
        
        public string Channel
        {
            get
            {
                return _channel;
            }
        }
        
        public Severity Severity
        {
            get
            {
                return _severity;
            }
        }
        
        public System.Collections.Generic.ICollection<LogEntryData> Data
        {
            get
            {
                return _data;
            }
        }
    }
}
